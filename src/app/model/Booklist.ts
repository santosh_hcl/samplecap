import { Book } from "./Book";

export let bookdatalist:Book[]=[
   new Book('Basic of JAVA','Herbert Schildt','English','Java',"Includes coverage of applets, servlets, swing, javaBeans, the AWT, and collections.",'IMG001',1),
   new Book('Programming with JAVA','E Balagurusamy','English','Java',"Java Programming A Primer vesion Servlets, Spring MVC.",'IMG002',2),
   new Book('HTML 5','Gowswamin','English','UI','HTML tags are keywords surrounded by angle brackets like <html> and normally come in pairs <html> </html>.','IMG005',3),
   new Book('ASP.NET MVC','Ibrahim','English','MVC','ASP.NET is a web application framework developed and marketed by Microsoft to allow programmers to build dynamic web sites. ','IMG006',4),
   new Book('Spring framework with JAVA','B. Mohamed Ibrahim','English','Java','The Spring Framework provides a comprehensive programming and configuration model. ','IMG003',5),
   new Book('Basic of JAVA','Herbert Schildt','English','Java','Includes coverage of applets, servlets, swing, javaBeans, the AWT, and collections.','IMG001',6)
]