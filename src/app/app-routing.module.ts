import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { CartComponent } from './component/cart/cart.component';
import { ProcrudComponent } from './component/procrud/procrud.component';
import { ProductsComponent } from './component/products/products.component';
import { IndecComponent } from './indec/indec.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path:'', redirectTo:'products',pathMatch:'full'},
  {path:'products', component: ProductsComponent},
  {path:'login',component: LoginComponent},
  {path:'register',component: RegisterComponent},
  {path:'cart', component: CartComponent},
  {path: 'procrud', component: ProcrudComponent},
  {path: 'indec', component: IndecComponent},
  {path: 'adminlogin', component: AdminloginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
